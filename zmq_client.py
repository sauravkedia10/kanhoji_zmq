#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 30 19:10:45 2021

@author: saurav
"""

import threading
import zmq
import time
import logging
logger = logging.getLogger(__name__)

class ZMQLocalClient(object):
    """
    Local lightweight super reliable ZMQ client called `local_client` which will 
    be attached to main trading application.
    
    It will have a corresponding paired listner which will housed in a separate
    app which will comminucate with Meru servers. That listner will listen
    to messages send by `local_client` and forward to Meru servers.
    
    We have not added a automatic reconnect feature as it is very stable.
    
    """
    def __init__(self, port=5556):
        """
        Initialize the local_client.

        Parameters
        ----------
        port : int, optional
            the port to which `local_client` will send messages to! 
            The default is 5556.

        Returns
        -------
        None.

        """
        context = zmq.Context()
        self.socket = context.socket(zmq.PAIR)
        self.socket.connect("tcp://localhost:%s" % port)
        time.sleep(2)
        
    def send_message(self, payload):
        """
        Method to send message to the Meru servers. This message will be sent
        to app running `ZMQClientReconnecting` class which will listen to these
        messages and in turn forward them to Meru Servers!

        Parameters
        ----------
        payload : dictionary
            python dict which contains the payload. This client will convert it
            to json automatically and send. Don't convert to json at your end.
            The client will also append the key `message_code` to this payload.   

        Returns
        -------
        None.

        """
        try:
            self.socket.send_json(payload)
        except:
            logger.exception(payload)
            
    def stop(self):
        """
        Stop this `local_client`. Run it at EOD only. Even if you don't stop it
        you should be fine.
        
        """
        try:
            self.socket.close()
        except Exception as e:
            logger.exception(e)
        

class ZMQClientReconnecting(object):
    """
    Reconnecting ZMQ client which will send messages to Meru Capitals.
    
    You will run this class as an independent application/process. In your 
    regular trading application, you will attach a `local_client`: a very 
    light ZMQ client of class `ZMQLocalClient` (written just above this one). 
    The `local_client` will send out messages (for Meru) using it `send_message`
    method. This client will intercept those messages and send it Meru's server.
    
    As a result, Your current trading application will run largely as it is with
    simple additions for `local_client`. All heavvy lifting will be done by this
    class which will run on a separate process and should it crash, or go it
    into some infinite error loop, it can simply be killed without impacting your
    main trading application.
    
    The `local_client` added to your trading application is a very light one
    and is unlikely to create any issues in your trading app.
    
    Notes
    ------
    1. Keep ports 5556 and 5557 free for ZMQ.
    
    2. This class has two ZMQ clients listening to two different ports, first which 
        listens to messages from `local_client' and the other which communicates 
        with Meru servers.
        
    3. It listens to messages from `local_client` on thread `MERU.ZMQ.Client.Local`
    
    3. This client start on `start_handler` call.
    
    4. It will stop on `stop_handler` call.
    
    5. It can send message to Meru using `send_message_threaded` call. However, in
        current implementation, it will listen to messages from `local_client`
        on its first ZMQ client, and once it gets a message there, it will
        forward it to Meru servers using `send_message_threaded` call in the
        second ZMQ client.
        
    6. For sake of simplicity, no response is being sent to `local_client` on 
        receipt of message, that can easily be changed.
    
    7. Sample payload to be sent to Meru servers is present in `send_sample_payload` 
        call. Use `send_sample_payload` call to test this client.
    
    8. Check whether the 2nd client (talking to Meru) is alive or not using 
        `is_client_alive` call. 1st client (talking to 'local_client') is very
        robust.

    9. `monitor_client` method is run at the time of starting this handler.
       It will spawn a thread named `MERU.ZMQ.Client.Monitor` which will
       check if client is connected or not every 10 seconds, if disconnected
       it will reconnect the client. DON'T KILL THIS THREAD.
    
    10. It will append a 'message_code' field in messages to give a unique idenitfier
        to each message sent for the day. Don't  provide a `message_code` 
        field in your payload. This client will add it automatically and 
        increment it for each message sent!
    
    11. `MERU.ZMQ.Client.Monitor` thread belongs to this client. Don't kill it!
    
    12. It exposes an attribute `messages_pending`. It can be used to identify
        pending messages. Especially if connection is terminated and can't be
        initiated again, this attr can be inspected to find pending messages.

    13. It exposes a callback `on_server_response` which can be used by you
        to run a callback when server responds after recieving the messages.
        It needs to overridden in your code.
        
    14. It owns two threads:
            - `MERU.ZMQ.Client.Local` : listens to messages from `local_client`
            - `MERU.ZMQ.Client.Monitor`: make sures ZMQ client talking to Meru 
                                         reconnects on close.
        DON'T KILL THEM!!
    
    """
    
    def __init__(self, 
            port_local = 5556,
            host='127.0.0.1',
            port=5557,
            username = b'admin',
            password = b'secret',

            ):
        """
        Initialize the ZMQClientReconnecting client.

        Parameters
        ----------
        
        port_local : int, optional
            The port on which `local_client` attached to main trading application
            is sending messages to. 
            The default is 5556.

        host : str.
            The server hostname (ip). The client will send message to this server at
            this host.
            The default is '127.0.0.1'.

        port : int, optional
            The port in server which is listening to this client. 
            The default is 5557.

        username : str, optional
            The username for this ZMQ connection. 
            The default is b'admin'.

        password : str, optional
            The password for this ZMQ connection. 
            The default is b'secret'.

        Returns
        -------
        None.
        
        Note
        ----
        The params host, port, username, password params will be provided
        by the team maintaining the server.

        """
        self.is_monitor_client = threading.Event()
        """
        threading.Event class flag.
        
        If it is set then it means this ZMQ client is being monitored and
        will be reconnected in case of disconnection.
        """
        self.is_monitor_client.set()
        self.port_local = port_local
        
        self.message_code = 1        
        """
        int
        Unique indenitfier for each message, auto managed by this client.
        
        It is inserted automatically in the payload by this client before 
        sending to server.
        """
        
        self.messages_pending = dict()
        """
        dict
            key: `message_code` generated by this client.
            value: full payload (itself a dict).
        """

        self.lock = threading.RLock()
        """
        threading.RLock instance.
        
        If there are multiple threads sending messages to the server at same time
        it raises an exception. To handle this we will Lock the client while
        sending a message and release it when response is recieved. Other 
        threads wanting to send the messages will have to wait for lock to
        be released before sending thier messages.
        """        
        self.username = username
        self.password = password
        self.host = host
        self.port = port
        
        # create the actual ZMQ client talking to Mrut servers. It will set as self.client.
        self.create_client()
        time.sleep(2)
        
        # Create local_client listening to messages from main trading application.
        self.create_local_client() 

        self.__version__ = 'v1a'
        
    def create_local_client(self):
        """
        Initialize the `local_client` listening to messages from main trading
        app and bind it to its port!

        Returns
        -------
        None.

        """
        try:
            context = zmq.Context()
            self.local_client = context.socket(zmq.PAIR)
            self.local_client.bind("tcp://*:%s" % self.port_local)
            time.sleep(2)
            self.thread_local = threading.Thread(target=self.on_message_local_client,
                name='MERU.ZMQ.Client.Local', daemon=True)
            self.thread_local.start()
        except Exception as e:
            logger.exception(e)
        
        
    def on_message_local_client(self):
        """
        Callback run when a message sent by the main trading application is
        recieved by the `local_client`.
        
        Currently it will send the message as it is to the Meru servers after
        appending a `message_code`.

        Returns
        -------
        None.

        """
        while self.is_monitor_client.is_set():
            try:
                payload = self.local_client.recv_json()
                self.send_message_threaded(payload=payload)
            except Exception as e:
                logger.exception(e)
            
        
        
    def _create_client(self):
        """
        Create a ZMQ client under the hood.

        Returns
        -------
        None.

        """
        try:
            self.client = zmq.Context.instance().socket(zmq.REQ)
            """
            Actual ZMQ client
            """
            self.client.plain_username = self.username
            self.client.plain_password = self.password

        except Exception as e:
            logger.exception(e)
             
        
    def create_client(self):
        """
        Create a ZMQ client under the hood in a non-blocking way.

        Returns
        -------
        None.

        """
        try:
            thread = threading.Thread(target=self._create_client, daemon=True)
            thread.start()
            time.sleep(1)
        
        except Exception as e:
            logger.exception(e)

    def start_handler(self):
        """
        Start this ZMQ client.
        
        It will connect to server.
        It will also start a thread with name `MERU.ZMQ.Client.Monitor` to 
        monitor if client is alive and reconnect, in case it is not alive.

        Returns
        -------
        None.

        """
        try:
            logger.warning("Starting the MERU ZMQ Client!")
            self.is_monitor_client.set()
            self.client.connect("tcp://%s:%s"%(self.host, self.port))
            self.monitor_client()
            time.sleep(2)
            # for message_code, payload in self.messages_pending.items():
            #     self.send_message_threaded(payload=payload)
                
        except Exception as e:
            logger.exception(e)        
        
    def stop_handler(self):
        """
        Stop this ZMQ client.
        
        It will also kill the thread which monitors and reconnects
        this client in case of disconnection.
        
        When this client is interfering with the code, just run this method and
        get rid of ZMQ client!

        Returns
        -------
        None.

        """
        try:    
            # clear the flag.
            self.is_monitor_client.clear()        
            self.client.close()
            self.thread_local.join()
        except Exception as e:
            logger.exception(e)
            
    def send_message(self, payload):
        """
        Send the message to the server. It is a blocking operation and will
        block till the time server responds. It will also run the callback
        `on_server_response` in a separate non-blocking thread when server
        responds.

        Parameters
        ----------
        payload : dictionary
            python dict which contains the payload. This client will convert it
            to json automatically and send. Don't convert to json at your end.
            The client will also append the key `message_code` to this payload.

        Returns
        -------
        message : dict
            keys:
                status: 'success' if recieved by server.
                message_code : message_code sent by client for this request. 
                    It is returned back so that client can reconcile that this
                    message is successfully recieved.
                    
        Note
        ----
        Consider using the method `send_message_threaded` to send messages in  
        non-blocking manner.
        
        """
        try:
            try:
                message_code = payload["message_code"]
            except:
                message_code = self.message_code
                self.message_code = self.message_code + 1
                payload['message_code'] = message_code
            
            with self.lock:
                try:
                    # add a new message to self.messages_pending
                    self.messages_pending[message_code] = payload
                    self.client.send_json(payload)
                    # once client sends the message over Zmq, it will block till the time server
                    # sends a reply.
                    message = self.client.recv_json()
                    
                    # if response from server recieved, it means all went well 
                    # and remove from self.messages_pending. 
                    self.messages_pending.pop(message_code)
            
                except zmq.error.ZMQError:
                    
                    self.client.close()
                    self.restart_on_close()
                    self.messages_pending[payload.get('message_code', 0)] = payload
                    # retun a failed message in case of error!
                    message = dict(
                        status="failed", 
                        message_code=payload.get('message_code', None)
                        )
                    logger.exception("Couldn't send this message: %s. Further Notes: "
                                     "\na) No attempts will be made to resend it!"
                                     "\nb) However the ZMQ client has been restarted and"
                                     " next message should be sent."
                                     "\nc)This failed messaged has been added to "
                                     "`self.messages_pending` attribute. You can manually send "
                                     "them through `self.send_message_threaded` method when "
                                     "client is back online!\n"%message)            
        
                except Exception as e:
                    logger.exception(e)
                    message = dict(
                        status="failed", 
                        message_code=payload.get('message_code', None)
                        )
                
                try:            
                    # run self.on_server_response callback in separate thread.
                    thread = threading.Thread(target=self.on_server_response,
                        kwargs={'message':message}, daemon=True)    
                    thread.start()
                except Exception as e:
                    logger.exception(e)
                    
        except Exception as e:
            logger.exception(e)
            message = dict(
                status="failed", 
                message_code=payload.get('message_code', None)
                        )
        
        return message
    

        
    def send_message_threaded(self, payload):
        """
        Send the message to server in non-blocking way by spawning a thread.
        
        It is a non-blocking operation. It will also run the callback
        `on_server_response` in a separate non-blocking thread when server
        responds.
        import datetime

        Parameters
        ----------
        payload : dictionary
            python dict which contains the payload. This client will convert it
            to json automatically and send. Don't convert to json at your end.
            The client will also append the key `message_code` to this payload.            

        Returns
        -------
        None.

        """
        try:
            thread = threading.Thread(target=self.send_message, 
                kwargs={'payload':payload}, daemon=True)
            thread.start()
        except Exception as e:
            logger.exception(e)
        
    # def send_stop_message(self):
    #     thread = threading.Thread(target=self.send_message, 
    #         kwargs={'payload':"STOP"}, daemon=True)            
    #     thread.start()
        
    
    def on_server_response(self, message):
        """
        The callback run automatically when server sends the response.
        
        The user can override it to run custom logic say log it or
        for reconciliation.

        Parameters
        ----------
        message : dict
            keys:
                status: 'success' if recieved by server.
                message_code : message_code sent by client for this request. 
                    It is returned back so that client can reconcile that this
                    message is successfully recieved.

        Returns
        -------
        None.

        """
        try:
            logger.warning(message)
        except Exception as e:
            logger.exception(e)
        
    def send_sample_payload(self, 
            payload=dict(
                    ticker = 'BANKNIFTY21JAN30600CE',
                    action = 'BUY',
                    quantity = 25,
                    strategy_code = 'STRATEGY1',
                    dealer = 'PARTNER1',
                    message_code = 1
                    )
                ):      
        """
        Send a sample message to server for testing purposes.

        Parameters
        ----------
        payload : dict, optional
            DESCRIPTION. 
            The default is dict(                    
                ticker = 'BANKNIFTY21JAN30600CE',                    
                action = 'BUY',                    
                quantity = 25,                    
                strategy_code = 'BPEARL',                    
                dealer = 'PARTNER1',                    
                message_code = 1                    
                ).

        Returns
        -------
        None.

        """
        try:
            self.send_message_threaded(payload=payload)
        except Exception as e:
            logger.exception(e)
        
    def is_client_alive(self):
        """
        Find if the ZMQ client is alive or not!

        Returns
        -------
        Boolean
            True if alive else False.

        """
        try:
            return not self.client.closed
        except Exception as e:
            logger.exception(e)
            return False
        
    
    def restart_on_close(self):
        """
        Find is the ZMQ client is alive or not. If it not alive, then connect it
        again.

        Returns
        -------
        None.

        """
        try:        
            if not self.is_client_alive():
                logger.warning("Restarting the MERU ZMQ client!")
                # create a new ZMQ client and connect it.
                self.create_client()
                self.start_handler()
        except Exception as e:
            logger.exception(e)
            
    def reconnect_on_close(self):
        """
        Run an inifinite loop which:
            a. Monitors every 10 seconds if ZMQ client is alive or not!
            b. Connect it again if it is not alive.
        
        It is a blocking operation, which will block the threar permanently.
        So run it in a separate thread using `monitor_client` call.

        Returns
        -------
        None.

        """
        try:        
            while self.is_monitor_client.is_set():
                self.restart_on_close()
                time.sleep(10)
        except Exception as e:
            logger.exception(e)
            
            
    def monitor_client(self):
        """
        Run a thread to monitor the ZMQ client every 10 second and reconnect
        if it got disconnected!
        
        It runs the method `reconnect_on_close` in a separate thread 
        `MERU.ZMQ.Client.Monitor` under the hood.

        Returns
        -------
        None.

        """
        try:
            threadname = 'MERU.ZMQ.Client.Monitor'
            threads = [thread.name for thread in threading.enumerate()]
            # launch monitoring thread only if not already running
            # useful when client reconnects.
            if threadname not in threads:
                self.is_monitor_client.set()        
                self.monitor_thread = threading.Thread(
                    target=self.reconnect_on_close,
                    daemon=True,
                    name=threadname
                    )
                self.monitor_thread.start()
        except Exception as e:
            logger.exception(e)

        
if __name__ == "__main__":
    # Initialize logging
    import logging
    formatter = "%(asctime)s.%(msecs)03d | %(module)s | %(levelname)s | %(lineno)d | %(message)s"
    DATE_FORMAT = '%H:%M:%S'    
    logging.basicConfig(format=formatter, datefmt=DATE_FORMAT)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)   
    
    # connect to server
    host = '127.0.0.1'
    client = ZMQClientReconnecting(host=host, port=5558, port_local=5559)
    Z = self = client
    
    # start the handler. It will spawn two threads mentioned earlier
    # it will listen to messages from main trading applications and forward
    # to Meru servers.
    # client.start_handler()
    
    # We can still send payload directly using this client.
    # client.send_sample_payload()
    
    # # OUR CUSTOM MESSAGE
    # payload1 = dict(                    
    #             ticker = 'BANKNIFTY21JAN30600CE',                    
    #             action = 'SELL',                    
    #             quantity = 25,                    
    #             strategy_code = 'STRATEGY1',                    
    #             dealer = 'E_PARTNER1'
    #             )    
    # client.send_message_threaded(payload=payload1)
    
    # # Stop handler at EOD!
    # Z.stop_handler()
    
            
        
        

