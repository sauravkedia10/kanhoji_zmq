#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  5 19:53:24 2019

@author: saurav

Authoritative AMQP documenatation
https://www.rabbitmq.com/amqp-0-9-1-reference.html#domain.no-wait

for username creation.

https://www.tutlane.com/tutorial/rabbitmq/rabbitmq-users

"""

# -*- coding: utf-8 -*-
# pylint: disable=C0111,C0103,R0205

import functools
import time
import pika
import logging
import json
import threading
import datetime
import os

logger = logging.getLogger(__name__)

HOST = 'localhost'
# HOST = '3.6.52.84'
PORT = 5672
VIRTUAL_HOST = 'meru_external_signals'
USER = 'kanhoji1'
PASSWORD = 'kanhojiuser1' 
EXCHANGE_NAME = ''

ROUTING_KEY = ('PARTNER1', 'PARTNER2')
NAME = 'MERU.EXTERNAL'

# not being used right now!!
DEALER_ID = 'PARTNER1'
APP_ID = 'PARTNER1'


class RabbitmqConsumer(object):
    """
    Demo RabbitmqConsumer for testing purposes with external providers.
    
    In AMPQ, the publisher have to publish to a given exchange with a given
    routing key. Consumers subscribing to those routing keys (through queues)
    get the messages.
    
    This consumer will not used directly. The ReconnectingRabbitMqConsumer
    will be used. The reason in that ReconnectingRabbitMqConsumer
    reconnects in case of rabbitmq failures. ReconnectingRabbitMqConsumer calls
    this class for actual subcription module and holds this class as its
    _consumer attribute.
    
    This class also has prefetch_count input which can be used to create
    multiple consumers.
    
    
    Consuming model:
    ========================
    
            
    Direct Exchange named 'MERUEXT' for all signal partners. 
        1. Each partner will post a message (order) with a routing key which will
                uniquely identify itself.
                    eg. Meru.PARTNER1
                        
        2. Consumers at Meru will have access to required message by subscribing
            to desired message patterns, ie through these routing key.
            
            
    Notes
    =====
    1. queue declaration and bindings is the job of consumers and will be handled
        here.
        
    2. As of now will will not purge or delete queues/exchanges. queue as well
        exchanges survive the closure of connections and channels that spawned 
        them and hence they need to be seperately managed. Meru follows
        a nightly reboot of server and hence these surviving rabbitmq services 
        will be automatically closed at the end of the day.
        
    3. This is a baseclass which will be subclassed by modules inside Meru.
        The routing key will come from those subclasses.
        
    4. When publisher in direct exchange publishes using a routing key to which 
        no queue is bound, that message is silenty dropped by rabbitmq. It is 
        not held in memory when a binding is done for that message later.
        
    5. The default implementations are done using:
        https://github.com/pika/pika/blob/master/examples/asynchronous_publisher_example.py
        https://github.com/pika/pika/blob/master/examples/asynchronous_consumer_example.py
        
    WARNING
    =======
    The self.__class__.__name__ of the superclass embedding this class is generally
    passed as _name input to this class. So, if there are multiple instances of
    that superclass, they will all have a common queue name and the messages 
    will be delivered to them in round robin/load balanced way. So, all instances
    will not recieve all messages but one message will be delivered to once
    instance only.
    
    So, when spawning multiple instances which uses consumers, be sure of usage
    pattern before using this consumer class. If you want all consumer to
    receive all messages then they must pass seperate name input at init time.
        
        
    AMQP
    ====
    Authoritative and comprehensive AMQP documenatation is available on:
        https://www.rabbitmq.com/amqp-0-9-1-reference.html    
    
    
    This is an example consumer that will handle unexpected interactions
    with RabbitMQ such as channel and connection closures.

    If RabbitMQ closes the connection, this class will stop and indicate
    that reconnection is necessary. You should look at the output, as
    there are limited reasons why the connection may be closed, which
    usually are tied to permission related issues or socket timeouts.

    If the channel is closed, it will indicate a problem with one of the
    commands that were issued and that should surface in the output as well.

    """


    def __init__(self, exchange=EXCHANGE_NAME, exchange_type='direct',
                 name=NAME, routing_key=ROUTING_KEY, prefetch_count=1):
        """
        Setup the Consumer object to connect to RabbitMQ.

        :exchange : name of the exchange, default "" for default exchange.
        :exchange_type: type of exchange eg. direct, topic etc.
        :name: name for this Publisher. Best to provide a name as that will
            be passed to thread it creates for publishing. It could be class
            name of consumer which uses this method. This can be very handy
            as one can look for whether this thread is running to check if 
            consumers is actively consuming or not.
            
            This name is also privided in app_id in the msessage properties it
            publishes, should consumer have interest in app which relayed it.
            
        :routing_key : routing_keys to which this consumer binds to.
            it is type set. Input could be either a string, or a set or tuple 
            or list. 
            Default = None, which means self._name is used as the only routing_key.            
                
        :prefetch_count = for qos. default at 1.

        
        Output
        ======
        It will store the basic params for this connection.
        
        It will create connection, and channel and declare the exchange when
        connect method is called.

        The queue name is predecided it will be 
            "Kahoji.%s.Queue.%s"%(self._name, self._exchange)
        We will have to review it if we increse the prefetch_counter number 
        later whether two consumers with same bindings can use the same queue
        name. Current implemenations allows seperate queues for each consumer
        should a module open multiple cosumers. 
        
        When this consumer disconnects because of errors inside rabbitmq, its
        should_reconnect attibute is set as True. In the ReconnectingRabbitMqConsumer
        which uses it, if this attribute is True, then consumer is created again
        else it is allowed to die. This is how auto-reconnect feature is 
        implemented.
        """
        self._exchange = exchange
        self._exchange_type = exchange_type
        self._name = self.__class__.__name__ if name is None else name   
        
        self._routing_key = set([self._name]) if routing_key is None else \
            set([routing_key]) if isinstance(routing_key, str) else set(routing_key)        
        
        
        self.should_reconnect = False
        self.was_consuming = False

        self._connection = None
        self._channel = None
        self._closing = False
        self._consumer_tag = None
        self._consuming = False
        # In production, experiment with higher prefetch values
        # for higher consumer throughput
        self._prefetch_count = prefetch_count
        if self._exchange != "":
            self._queue = "Meru.%s.Queue.%s"%(self._name, self._exchange)        

    def connect(self):
        """This method connects to RabbitMQ, returning the connection handle.
        When the connection is established, the on_connection_open method
        will be invoked by pika.

        :rtype: pika.SelectConnection

        """
        logger.debug('%s Rabbitmq: connecting...'%(self._name))
        
        credentials = pika.PlainCredentials(USER, PASSWORD)
        self.parameters = pika.ConnectionParameters(
                host=HOST,
                port=PORT,
                virtual_host=VIRTUAL_HOST,
                credentials=credentials,
                connection_attempts=3)     
        
        return pika.SelectConnection(
            parameters= self.parameters,
            on_open_callback=self.on_connection_open,
            on_open_error_callback=self.on_connection_open_error,
            on_close_callback=self.on_connection_closed)        


    def close_connection(self):
        self._consuming = False
        if self._connection.is_closing or self._connection.is_closed:
            logger.debug('%s Rabbitmq Connection is closing or already closed!', self._name)
        else:
            logger.info('%s Rabbitmq closing connection', self._name)
            self._connection.close()

    def on_connection_open(self, _unused_connection):
        """This method is called by pika once the connection to RabbitMQ has
        been established. It passes the handle to the connection object in
        case we need it, but in this case, we'll just mark it unused.

        :param pika.SelectConnection _unused_connection: The connection

        """
        logger.debug('%s Rabbitmq Connection opened!'%self._name)
        self.open_channel()

    def on_connection_open_error(self, _unused_connection, err):
        """This method is called by pika if the connection to RabbitMQ
        can't be established.

        :param pika.SelectConnection _unused_connection: The connection
        :param Exception err: The error

        """
        logger.error('%s Rabbitmq connection open failed, %s'%(self._name, err))        
        self.reconnect()

    def on_connection_closed(self, _unused_connection, reason):
        """This method is invoked by pika when the connection to RabbitMQ is
        closed unexpectedly. Since it is unexpected, we will reconnect to
        RabbitMQ if it disconnects.

        :param pika.connection.Connection connection: The closed connection obj
        :param Exception reason: exception representing reason for loss of
            connection.

        """
        self._channel = None
        if self._closing:
            self._connection.ioloop.stop()
        else:
            logger.warning('%s Rabbitmq Connection closed, reconnect necessary: %s'%(self._name, reason))            
            self.reconnect()

    def reconnect(self):
        """Will be invoked if the connection can't be opened or is
        closed. Indicates that a reconnect is necessary then stops the
        ioloop.

        """
        self.should_reconnect = True
        self.stop()

    def open_channel(self):
        """Open a new channel with RabbitMQ by issuing the Channel.Open RPC
        command. When RabbitMQ responds that the channel is open, the
        on_channel_open callback will be invoked by pika.

        """
        logger.debug('%s Rabbitmq Creating a new channel.'%self._name)
        self._connection.channel(on_open_callback=self.on_channel_open)

    def on_channel_open(self, channel):
        """This method is invoked by pika when the channel has been opened.
        The channel object is passed in so we can make use of it.

        Since the channel is now open, we'll declare the exchange to use.

        :param pika.channel.Channel channel: The channel object

        """
        logger.info('%s Rabbitmq Consuming Channel opened!'%self._name)
        self._channel = channel
        self.add_on_channel_close_callback()
        if self._exchange != "":
            self.setup_exchange()        
        else:
            # Declare the queues
            for routing_key in self._routing_key:
                self._channel.queue_declare(queue=routing_key)
            self.set_qos()
                
    def add_on_channel_close_callback(self):
        """This method tells pika to call the on_channel_closed method if
        RabbitMQ unexpectedly closes the channel.

        """
        logger.debug('%s Rabbitmq Adding channel close callback.'%self._name)
        self._channel.add_on_close_callback(self.on_channel_closed)

    def on_channel_closed(self, channel, reason):
        """Invoked by pika when RabbitMQ unexpectedly closes the channel.
        Channels are usually closed if you attempt to do something that
        violates the protocol, such as re-declare an exchange or queue with
        different parameters. In this case, we'll close the connection
        to shutdown the object.

        :param pika.channel.Channel: The closed channel
        :param Exception reason: why the channel was closed

        """
        logger.warning('%s Rabbitmq Channel %s was closed: %s'%(self._name, 
                                                        channel, reason))
        self.close_connection()

    def setup_exchange(self):
        """Setup the exchange on RabbitMQ by invoking the Exchange.Declare RPC
        command. When it is complete, the on_exchange_declareok method will
        be invoked by pika.

        :param str|unicode exchange_name: The name of the exchange to declare

        """
        logger.debug('%s Rabbitmq Declaring exchange %s'%(self._name, self._exchange))
#        # Note: using functools.partial is not required, it is demonstrating
#        # how arbitrary data can be passed to the callback when it is called
#        cb = functools.partial(
#            self.on_exchange_declareok, userdata=exchange_name)
#        self._channel.exchange_declare(
#            exchange=exchange_name,
#            exchange_type=self.EXCHANGE_TYPE,
#            callback=cb)
        
        self._channel.exchange_declare(
            exchange=self._exchange,
            exchange_type=self._exchange_type,
            callback=self.on_exchange_declareok
            # passive=True, durable=False, auto_delete=False
            )        
        

    def on_exchange_declareok(self, _unused_frame):
        """Invoked by pika when RabbitMQ has finished the Exchange.Declare RPC
        command.

        :param pika.Frame.Method unused_frame: Exchange.DeclareOk response frame
        :param str|unicode userdata: Extra user data (exchange name)

        """
        logger.debug('%s Rabbitmq Exchange declared: %s'%(self._name, 
                                                          self._exchange))
        self.setup_queue(self._queue)

    def setup_queue(self, queue_name):
        """Setup the queue on RabbitMQ by invoking the Queue.Declare RPC
        command. When it is complete, the on_queue_declareok method will
        be invoked by pika.

        :param str|unicode queue_name: The name of the queue to declare.

        """
        logger.debug('%s Rabbitmq Declaring queues!'%self._name)
#        cb = functools.partial(self.on_queue_declareok, queue_name=queue_name)
        self._channel.queue_declare(queue=self._queue, 
                                    callback=self.on_queue_declareok)

    def on_queue_declareok(self, _unused_frame):
        """Method invoked by pika when the Queue.Declare RPC call made in
        setup_queue has completed. In this method we will bind the queue
        and exchange together with the routing key by issuing the Queue.Bind
        RPC command. When this command is complete, the on_bindok method will
        be invoked by pika.

        :param pika.frame.Method _unused_frame: The Queue.DeclareOk framexxxxxx
        :param str|unicode userdata: Extra user data (queue name)

        """
        for routing_key in self._routing_key:
            logger.debug('%s Rabbitmq Binding %s to queue %s with routing key %s', 
                     self._name, self._exchange, self._queue, routing_key)        
            self._channel.queue_bind(
                queue=self._queue,
                exchange=self._exchange,
                routing_key=routing_key
                )
        self.set_qos()

#    def on_bindok(self, _unused_frame):
#        """Invoked by pika when the Queue.Bind method has completed. At this
#        point we will set the prefetch count for the channel.
#
#        :param pika.frame.Method _unused_frame: The Queue.BindOk response frame
#        :param str|unicode userdata: Extra user data (queue name)
#
#        """        
#        self.set_qos()

    def set_qos(self):
        """This method sets up the consumer prefetch to only be delivered
        one message at a time. The consumer must acknowledge this message
        before RabbitMQ will deliver another one. You should experiment
        with different prefetch values to achieve desired performance.

        """
        self._channel.basic_qos(
            prefetch_count=self._prefetch_count,
            callback=self.on_basic_qos_ok)
 
    def on_basic_qos_ok(self, _unused_frame):
        """Invoked by pika when the Basic.QoS method has completed. At this
        point we will start consuming messages by calling start_consuming
        which will invoke the needed RPC commands to start the process.

        :param pika.frame.Method _unused_frame: The Basic.QosOk response frame

        """
        self.start_consuming()

    def start_consuming(self):
        """
        This method sets up the consumer by first calling
        add_on_cancel_callback so that the object is notified if RabbitMQ
        cancels the consumer. It then issues the Basic.Consume RPC command
        which returns the consumer tag that is used to uniquely identify the
        consumer with RabbitMQ. We keep the value to use it when we want to
        cancel consuming. The on_message method is passed in as a callback pika
        will invoke when a message is fully received.

        in Meru, we are using auto_ack is True, however, if we keep 
        auto_ack = False, we need to add self.acknowledge_message(basic_deliver.delivery_tag)
        step somewhere either in on_message or on_message_do block.
        
        """
        self.add_on_cancel_callback()
        if self._exchange != "":
            self._consumer_tag = self._channel.basic_consume(
                queue=self._queue, 
                on_message_callback=self.on_message,
                auto_ack=True
                )
        else:
            for routing_key in self._routing_key:
                self._consumer_tag= self._channel.basic_consume(
                    queue=routing_key, 
                    on_message_callback=self.on_message,
                    auto_ack=True
                    )
        self.was_consuming = True
        self._consuming = True

    def add_on_cancel_callback(self):
        """Add a callback that will be invoked if RabbitMQ cancels the consumer
        for some reason. If RabbitMQ does cancel the consumer,
        on_consumer_cancelled will be invoked by pika.

        """
        self._channel.add_on_cancel_callback(self.on_consumer_cancelled)

    def on_consumer_cancelled(self, method_frame):
        """Invoked by pika when RabbitMQ sends a Basic.Cancel for a consumer
        receiving messages.

        :param pika.frame.Method method_frame: The Basic.Cancel frame

        """

        if self._channel:
            self._channel.close()

    def on_message(self, _unused_channel, basic_deliver, properties, body):
        """Invoked by pika when a message is delivered from RabbitMQ. The
        channel is passed for your convenience. The basic_deliver object that
        is passed in carries the exchange, routing key, delivery tag and
        a redelivered flag for the message. The properties passed in is an
        instance of BasicProperties with the message properties and the body
        is the message that was sent.

        :param pika.channel.Channel _unused_channel: The channel object
        :param pika.Spec.Basic.Deliver: basic_deliver method
        :param pika.Spec.BasicProperties: properties
        :param bytes body: The message body

        smaple properties: {'content_type': 'application/json', 
            'content_encoding': None, 'headers': None, 'delivery_mode': None, 
            'priority': None, 'correlation_id': None, 'reply_to': None, 
            'expiration': None, 'message_id': '0', 
            'timestamp': 1612343765348583, 'type': 'SIGNAL', 
            'user_id': 'kanhoji1', 'app_id': 'ALGOTEMPLE', 'cluster_id': None}            
                           
        sample basic_deliver: {'consumer_tag': 'ctag1.7cb300d1d9c34aed848f42ce31b52d8c', 
            'delivery_tag': 1, 'redelivered': False, 'exchange': 'MERUEXTERNAL', 
            'routing_key': 'ALGOTEMPLE'}

        """
        try:
            logger.debug('Received message # %s from %s: %s',
                        basic_deliver.delivery_tag, properties.app_id, body)
            
            # routing_key and exchange to which the message has been routed can be
            # extracted by basic_deliver.routing_key, basic_deliver.exchange 
            # https://pika.readthedocs.io/en/stable/modules/spec.html for details
            # on data carried by basic_deliver and the properties objects here.
            
    #        logger.warning("%s, %s", basic_deliver.routing_key, basic_deliver.exchange)
    #        try:
            logger.debug(properties.__dict__)
            logger.debug(basic_deliver.__dict__)
            
            self._properties = properties
            self._basic_deliver = basic_deliver
            
            self.on_message_do(
                        body=json.loads(body), 
                        properties=properties,
                        routing_key=basic_deliver.routing_key,
                        exchange=basic_deliver.exchange)
        
        
        except Exception as e:
            logger.exception (e)
        
    def on_message_do(self, body, properties, routing_key, exchange):
        """
        The on_message_do method can be set seperately by the calling
        subclass to determine what to do with the message body and message
        properties.
        
        on_message_do will recieve two params an input:
            a. body: unpacked or deserialized body/message
            b. properties: message properties from rabbitmq.
                                pika.spec.BasicProperties
            c. routing_key: routing_key with which message was pushed.
            d. exchange: exchange to which message was pushed.
            
        # https://pika.readthedocs.io/en/stable/modules/spec.html for details
        # on data carried by basic_deliver and the properties objects.            
            
        """
        
        logger.debug('%s Rabbitmq Received message %s from %s using routing_key '\
                     '%s and exchange %s!', 
                     body,
                     properties.app_id,
                     routing_key, 
                     exchange)
        
        
        
#        if auto_ack is False then use this:
#        self.acknowledge_message(basic_deliver.delivery_tag)

    def acknowledge_message(self, delivery_tag):
        """Acknowledge the message delivery from RabbitMQ by sending a
        Basic.Ack RPC method for the delivery tag.

        :param int delivery_tag: The delivery tag from the Basic.Deliver frame
        
        Required only if auto_ack = False in basic_consume method.

        """
        logger.debug('Acknowledging message %s', delivery_tag)
        self._channel.basic_ack(delivery_tag)

    def stop_consuming(self):
        """Tell RabbitMQ that you would like to stop consuming by sending the
        Basic.Cancel RPC command.

        """
        if self._channel:

            cb = functools.partial(
                self.on_cancelok, userdata=self._consumer_tag)
            self._channel.basic_cancel(self._consumer_tag, cb)

    def on_cancelok(self, _unused_frame, userdata):
        """This method is invoked by pika when RabbitMQ acknowledges the
        cancellation of a consumer. At this point we will close the channel.
        This will invoke the on_channel_closed method once the channel has been
        closed, which will in-turn close the connection.

        :param pika.frame.Method _unused_frame: The Basic.CancelOk frame
        :param str|unicode userdata: Extra user data (consumer tag)

        """
        self._consuming = False
#        logger.debug(
#            'RabbitMQ acknowledged the cancellation of the consumer: %s',
#            userdata)
        self.close_channel()

    def close_channel(self):
        """Call to close the channel with RabbitMQ cleanly by issuing the
        Channel.Close RPC command.

        """
        logger.warning('%s Rabbitmq closing the channel', self._name)
        self._channel.close()
        
    def purge_queue(self):
        """
        Purge queues defined in qname_outs attr
        
        """
        logger.debug('%s Rabbitmq purging all queues to which this publisher is publishing.'%self._name)
        
        if isinstance(self._queue, str):
            self._purge_queue(self._queue)

        else:
            for qname_out in self._queue:
                self._purge_queue(qname_out)
                
    def _purge_queue(self, qname_out):                 
        logger.debug('%s Rabbitmq purging queue %s.', self._name, qname_out)

        self._channel.queue_purge(qname_out)   

    # the methods below dont work
        
#    def unbind_queue(self):
#        logger.debug('%s Rabbitmq unbinding all queues to which this publisher is publishing.'%self._name)        
#        if isinstance(self._queue, str):
#            self._unbind_queue(self._queue)
#
#        else:
#            for qname_out in self._queue:
#                self._unbind_queue(qname_out)
#        
#    def _unbind_queue(self, qname_out):
#        logger.debug('%s Rabbitmq unbinding queue %s.', self._name, qname_out)        
#        if self._exchange != "":
#            self._channel.queue_unbind(qname_out,
#                                       exchange=self._exchange)
#        
#    def delete_queue(self):
#        logger.debug('%s Rabbitmq deleting all queues to which this publisher is publishing.'%self._name)        
#        
#        if isinstance(self._queue, str):
#            self._delete_queue(self._queue)
#
#        else:
#            for qname_out in self._queue:
#                self._delete_queue(qname_out)
#    
#    def _delete_queue(self, qname_out):
#        logger.warning('%s Rabbitmq deleting queue %s.', self._name, qname_out)       
#        self._closing = True        
#        self._channel.queue_delete(qname_out)
#        
    
    def delete_exchange(self, exchange=None):
        
        exchange = exchange or self._exchange 
        self._channel.exchange_delete(exchange=exchange)
        logger.warning('%s Rabbitmq deleting exchange %s.', self._name, exchange)                
        

    def run(self):
        """Run the example consumer by connecting to RabbitMQ and then
        starting the IOLoop to block and allow the SelectConnection to operate.

        """
        self._connection = self.connect()
        self._connection.ioloop.start()

    def run_in_thread(self):
        self.threadname = "MERU." + self._exchange + '.' + self._name + ".Rabbitmq.Consumer"        
        self.thread = threading.Thread(target=self.run,
                                       name=self.threadname,
                                       daemon=True)
        self.thread.start()        

    def stop(self):
        """Cleanly shutdown the connection to RabbitMQ by stopping the consumer
        with RabbitMQ. When RabbitMQ confirms the cancellation, on_cancelok
        will be invoked by pika, which will then closing the channel and
        connection. The IOLoop is started again because this method is invoked
        when CTRL-C is pressed raising a KeyboardInterrupt exception. This
        exception stops the IOLoop which needs to be running for pika to
        communicate with RabbitMQ. All of the commands issued prior to starting
        the IOLoop will be buffered but not processed.

        """
        if not self._closing:
            self._closing = True
            logger.debug('%s Rabbitmq Stopping!', self._name)
            if self._consuming:
                self.stop_consuming()
#                self._connection.ioloop.start()
#            else:
#                self._connection.ioloop.stop()
                logger.debug('%s Rabbitmq Stopped!', self._name)
        self._connection.ioloop.stop()
        
            

class ReconnectingRabbitMqConsumer(object):
    """
    This is an example consumer that will reconnect if the nested
    RabbitMqConsumer indicates that a reconnect is necessary. This is the 
    one which will be actually called by Meru modules. It holds the 
    nested RabbitMqConsumer  in its _consumer attribute. When its rabbitmq
    consumer stops abruptly, its should_reconnect attribute is set to True,
    and in the run method of this class, a new nested consumer with same 
    attributes is created and connected.
 
    """

    def __init__(self, exchange=EXCHANGE_NAME, exchange_type='direct',
                 name=NAME, routing_key=ROUTING_KEY,
                 on_message_callback=None,
                 prefetch_count=1
                 ):   
        """
        Setup the Consumer object, we will use
        to connect to RabbitMQ.

        :exchange : name of the exchange, default "" for default exchange.
        :exchange_type: type of exchange eg. direct, topic etc.
        :name: name for this Publisher. Best to provide a name as that will
            be passed to thread it creates for publishing. It could be class
            name of consumer which uses this method. This can be very handy
            as one can look for whether this thread is running to check if 
            consumers is actively consuming or not.
            
            This name is also privided in app_id in the msessage properties it
            publishes, should consumer have interest in app which relayed it.
            
        :routing_key : routing_keys to which this consumer binds to.
            it is type set. Input could be either a string, or a set or tuple 
            or list. 
            Default = None, which means self._name is used as the only routing_key.        
            
        :prefetch_count = for qos. default at 1.
        
        Output
        ======
        It will store the basic params for this connection.
        
        It will create connection, and channel and declare the exchange when
        connect method is called.

        The queue name is predecided it will be "Kahoji.%s.Queue"%self._name
        We will have to review it if we increse the prefetch_counter number 
        later whether two consumers with same bindings can use the same queue
        name.
        
        When this consumer disconnects because of errors inside rabbitmq, its
        should_reconnect attibute is set as True. In the ReconnectingRabbitMqConsumer
        which uses it, if this attribute is True, then consumer is created again
        else it is allowed to die. This is how auto-reconnect feature is 
        implemented.        
        """
        
        self._reconnect_delay = 0
        self._exchange = exchange
        self._exchange_type = exchange_type
        self._name = self.__class__.__name__ if name is None else name   
        self._routing_key = set([self._name]) if routing_key is None else \
            set([routing_key]) if isinstance(routing_key, str) else set(routing_key)        
        self.prefetch_count = prefetch_count
        
        self._consumer = RabbitmqConsumer( 
            exchange=self._exchange, exchange_type=self._exchange_type, 
            name=self._name, routing_key=self._routing_key,
            prefetch_count=self.prefetch_count)
        
        self.on_message_callabck = self.on_message if on_message_callback is \
            None else on_message_callback
            
        self._stopping = False
        
    @property
    def has_opened(self):
        """
        Has this consumer's channel opened up atlest once after it being started.


        Returns
        -------
        Boolean True or False
            1. When the consumer is initiated but channel not connected yet, it will
            return False.
            2. When the consumer is initiated and channel is connected, it will
            return True.
            3. When the consumer is initiated and channel is connected and later 
            on it gets connected and disconnected or is stopped at End of Day,
            output is uncertain (because of bug ins top_consuming)
        
        The use case of this method is limited. This consumer, when intiliazed,
        will return False for this method. When start_consuming method is called,
        and if a successful connection is made, it will return True for the first
        time then. The use of this method is limited to this flip from False
        to True for the first time only.
        
        When an object which uses this method is being initilized, it may need
        to wait on a successful rabbitmq consumer connection (using this class)
        before it can be initialized fully. In this case, the object can wait
        in an endless While loop while this method returns True. Once it returns
        True, it means a successful rabbitmq consumer connection has been made
        and then rest of init step in the object can continue.
        
        WARNING
        -------
        Use this method very very carefully and only when initializing or starting
        the downstream objects which uses this class.        
        
        IDEALLY DONT USE IT!
        
        """
        try:
            return self._consumer._channel.is_open
        except:
            return False
            
    def on_message(self, body, properties, routing_key, exchange):
        """
        The nested consumer object passes on the callback on_message_do to
        on_message method. The on_message_do method has four inputs
        
        :body: derserialized body 
        :properties: message properties from rabbitmq. Its a 
            pika.spec.BasicProperties object.
        :routing_key: routing_key with which message was pushed.
        :exchange: exchange to which message was pushed.
        
        The on_message_do method has to be overrided by this module and as well
        as subsequent Meru modules to process the messages received.
        
        When the consumer is started, its on_message_do will be overridden
        by this method.
        
        """
        logger.debug('Rabbitmq Received message %s from %s using routing_key '\
                     '%s and exchange %s!', 
                     body,
                     properties.app_id,
                     routing_key, 
                     exchange)

    def run(self):
        """
        Override the on_message_do method of nested consumer, 
        start the nested _cosmerer,
        if it fails abruputly, create a new consumer and start again else exit.
        
        This is a blocking method, if we want unblocking method use run_in_thread.
        """
        
        while True:
            self._consumer.on_message_do = self.on_message_callabck            
            self._consumer.run()
            # in case of disconnect, try to reconnect.
            # for errors inside Rabbitmq, self._consumer.should_reconnect will
            # be True
            self._maybe_reconnect()
            # if there connection was stopped by this consumer, these 
            # self._stopping attribute of this class will be set to True.
            # Don't restart in that case and exit out of the loop.
            if self._stopping:
                break
            
    def run_in_thread(self):
        """
        Run the consumer in non-blocking or in a separate thread.
        
        """
        
        self.threadname = "MERU." + self._exchange + '.' + self._name + ".Rabbitmq.Consumer"
        self.thread = threading.Thread(target=self.run,
                                       name=self.threadname,
                                       daemon=True)
        self.thread.start()              
            

#    def _maybe_reconnect(self):
#        thread = threading.Thread(target=self.__maybe_reconnect,
#                                  dameon=True)
        
    def _maybe_reconnect(self):
        """
        Create a new nested consumer in case connection breaks and if should_reconnect
        is True. However, the new consumer will be started by run method.
        
        """
        
        if self._consumer.should_reconnect:
            self._consumer.stop()
            reconnect_delay = self._get_reconnect_delay()
            logger.warning('Reconnecting after %d seconds', reconnect_delay)
            time.sleep(reconnect_delay)
            self._consumer = RabbitmqConsumer(
                    exchange=self._exchange, exchange_type=self._exchange_type, 
                    name=self._name, routing_key=self._routing_key,
                    prefetch_count=self.prefetch_count)
            

    def _get_reconnect_delay(self):
        if self._consumer.was_consuming:
            self._reconnect_delay = 0
        else:
            self._reconnect_delay += 1
        if self._reconnect_delay > 30:
            self._reconnect_delay = 30
        return self._reconnect_delay
    
    def start_consuming(self):
        """
        Start cosuming from the rabbitmq over a thread.
        
        """
        # in case of disconnections, self._stopping is checked, 
        # reconnection only happens when this is False.
        # if disconnection is initiated by the consumer/Meru, set it as True
        self._stopping = False
        self.run_in_thread()
        
    def stop_consuming(self):
        """Stop after 5 seconds to allow any pending messages to be consumed first"""
        self.stop_thread = threading.Timer(interval=5, 
                                           function=self._stop_consuming
                                           )
        self.stop_thread.start()
        
    def _stop_consuming(self):
        """
        Stop cosuming from the rabbitmq over a thread. Close channel and 
        connection, however exchange and queue survives.

        """
        # in case of disconnections, self._stopping is checked, 
        # reconnection only happens when this is False.
        # if disconnection is initiated by the consumer/Meru, set it as True
  
        self._stopping = True
        self._consumer.stop()
        


class DemoDefaultConsumer(object):
    """
    DemoDefaultConsumer is sort of demonstration that how a Rabbitmq consumer
    which auto restarts and reconnects on rabbitmq originated errors can be 
    implemented inside Meru's module.
    
    DemoDefaultConsumer could be any of the modules of the Meru and to use
    the consumer they will create an attribute rconsumer which is the actual
    ReconnectingRabbitMqConsumer object for this module.
    
    """
    
  
    def __init__(self, 
                 rabbitmq_exchange=EXCHANGE_NAME, 
                 rabbitmq_exchange_type='direct',
                 rabbitmq_name=NAME, 
                 rabbitmq_routing_key=ROUTING_KEY,
                 rabbitmq_prefetch_counter=1
                 ):
        
        """
        :rabbitmq_exchange = rabbitmq_exchange name 
        :rabbitmq_exchange_type = exchange_type, default 'direct'
        :rabbitmq_name = name for this Publisher. Best to provide a name as that will
            be passed to thread it creates for publishing. It could be class
            name of consumer which uses this method. This can be very handy
            as one can look for whether this thread is running to check if 
            consumers is actively consuming or not.
            
            This name is also privided in app_id in the msessage properties it
            publishes, should consumer have interest in app which relayed it.This name gets attached 
        
            default in class name. It should be fine in most cases.
            
        :rabbitmq_routing_key: routing_keys to which this consumer binds to.
            it is type set. Input could be either a string, or a set or tuple 
            or list. 
            Default = None, which means self.__class__.__name__ is used as the only routing_key.            
                
        :rabbitmq_prefetch_counter = for qos, default is 1.
            
            
        """
    
        rabbitmq_name = self.__class__.__name__ if rabbitmq_name is None else \
                                                                rabbitmq_name                  
        self.rconsumer = ReconnectingRabbitMqConsumer(
                    name=rabbitmq_name,
                    exchange=rabbitmq_exchange,
                    exchange_type=rabbitmq_exchange_type,
                    routing_key=rabbitmq_routing_key,
                    on_message_callback=self.on_message,
                    prefetch_count=rabbitmq_prefetch_counter
                    )
        
    def start_consuming(self):
        """
        The ioloop is blocking and hence run in thread.
        """
        self.rconsumer.start_consuming()
        
    def on_message(self, body, properties, routing_key, exchange):
        try:
            logger.info('Rabbitmq Received message %s from %s using routing_key '\
                         '%s and exchange %s!', 
                         body,
                         properties.app_id,
                         routing_key, 
                         exchange)
        except:
            logger.exception (body)
    
        
    def stop_consuming(self):
        """
        Stop the consumer. Cancels the channel and connects but exchanges and
        queues survive.
        """
        self.rconsumer.stop_consuming()


class DemoDefaultConsumer2(DemoDefaultConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__ (*args, **kwargs)
        

if __name__ == '__main__':
    import logging
    formatter = "%(asctime)s.%(msecs)03d | %(module)s | %(levelname)s | %(lineno)d | %(message)s"
    DATE_FORMAT = '%H:%M:%S'    
    logging.basicConfig(format=formatter, datefmt=DATE_FORMAT)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
     
    sub = DemoDefaultConsumer()
    sub.start_consuming()
    self = sub.rconsumer._consumer
    