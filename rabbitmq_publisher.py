
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  5 12:23:55 2019

@author: saurav

NOTES
-----
1. Authoritative AMQP documenatation
https://www.rabbitmq.com/amqp-0-9-1-reference.html#domain.no-wait

2. MESSAGE IS SIMPLY AN ORDER SENT BY YOU! In programming term we 
    call it a message!

3. For the documentation, you means our algo partner who is sending us 
    the signals and us/we/our means Meru Capitals!
    
4. We are sending this you as a proprietory code for handling our 
    interactions. Please don't distribute it without prior statement!
    
"""

import functools
import logging
import json
import pika
import zmq
import time
import threading
import datetime
import os
from kanhoji_zmq.meru_configuration import (PORT_LOCAL, HOST, PORT, VIRTUAL_HOST,
    USER, DEALER_ID, APP_ID, ROUTING_KEY, NAME, PASSWORD, EXCHANGE_NAME, 
    PRODUCT_MIS, PRODUCT_NRML
)

logger = logging.getLogger(__name__)


class ZMQLocalClient(object):
    """
    Local lightweight super reliable ZMQ client called `local_client` 
    which will be attached to your main trading application.
    
    It will have a corresponding paired listner which will be housed in a separate
    app which will communicate with Meru (our) servers. That listner will listen
    to message/order send by `local_client` attached to your trading app 
    and forward to our servers.
    
    We have not added a automatic reconnect feature as it is very stable.
    
    """
    def __init__(self, port=5556):
        """
        Initialize the local_client.

        Parameters
        ----------
        port : int, optional
            the port to which `local_client` will send messages to! 
            The default is 5556.

        Returns
        -------
        None.

        """
        self.message_id = 1
        """int
        
        A auto incrementing message_id which we will seeded to each message
        sent by you. The purpose is to assign a unique key to each message.
        
        It will help in reconciliation at both your end and ours.
        
        You can ignore it!
        """
        
        context = zmq.Context()
        self.socket = context.socket(zmq.PAIR)
        self.socket.connect("tcp://localhost:%s" % port)
        time.sleep(2)
        
    def meru_place_order(self, ticker, action, strategy_id,
            quantity=0,
            price=0,
            trigger_price=0,
            product=PRODUCT_MIS,
            tag=None,
            timestamp=int(datetime.datetime.now().timestamp() * 1000000),
            ):
        """
        Method to send new order message to the Meru servers. 
        
        This message/order will be sent to this app running `RabbitmqReconnectingPublisher` 
        class  which  will listen to these messages/orders and in turn forward them 
        to Meru Servers!

        Parameters
        ----------
        ticker : str
            The ticker as per ZEORDHA terminology.
            avialable at  https://api.kite.trade/instruments

        action : {'BUY', 'SELL'}
            'BUY' or 'SELL'            
        
        strategy_id : str, of max length 16 chars
            example: 'ALGO1'
            This single app `MeruApp` will handle messages from all your 
            strategies which are live with Meru. In order to map various orders
            to thier right strategies, orders should contain this field 
            `strategy_id`. 
            
            Please agree on `stategy_id` with Meru.
                    
        quantity : int, optional
            The actual quantity to be bought or sold.
            If it is a derivative ticker, still provide number of shares and 
            not number of lots! For example, for trading two lots on BANKNIFTY 
            futures where each lot is of qty 25, send `quantity` param as 50.
        
            The default is 0, which is provided for only for testing purposes,
            i.e. testing communication between Meru and your app.
            
        price : int, optional
            The `price` param for order. 
            The default is 0.
            
        trigger_price : int, optional
            The `price` param for order. 
            The default is 0.
            
            if price==0 and trigger_price==0: means MARKET order
            if price>0 and trigger_price==0: means LIMIT order
            if price==0 and trigger_price>0: means SL_M order
            if price>0 and trigger_price>0: means SL order
            
            The order_type is not being sent separately as it can be deduced
            from logic above.
                    
        product : {'MIS','NRML'}, optional
            Product type for the order. 
            The default is `MIS`.
            
        tag : str/int of max length 16 chars. (optional)
            A tag (say order_id) of your choice for the order being 
            placed. It should originate from your trading application.
            It is a free field assigned to you for sending your custom
            field.
            
            The default in None
            
        timestamp : int, optional
            The UTC unix (epoch) timestamp upto microsecond resolution. 
            
            The default is current timestamp. We multiple the timestamp with
            1,000,000 to convert it to an int!
            
        Returns
        -------
        int
            message_id:  
                unique `message_id` for this message/order.
                0 if there was an exception in sending message!
        
        Note
        ----
        Some fields are going to be auto-appended to the message when it is
        being sent by the RabbitMQ client like `message_id`, `dealer_id`.
    
        Incidently, (`message_id`, `dealer_id`) are reserved keywords and 
        don't use them in in this class.
            
            `message_id`: A auto incrementing unqiue key for each message
                sent by you. This is maintained by this 'ZMQLocalClient` class 
                and you don't have to do anything.
                
                It will help in reconciliation at both your end and ours.

            `dealer_id` : dealer_id assigned to you by us. It will help us 
                indentify which partner sent us this message as multiple partners
                might be running their algos with us.                .

        """
        
        assert action in ('BUY', 'SELL'), ('param `action` should be '
            'either "BUY" or "SELL". Inputted: %s'%action)
            
        assert isinstance (quantity, int) and (quantity >= 0), ('param `quantity` '                                                                
            'should be a positive integer >=0. Inputted: %s'%quantity)
            
        assert isinstance (price, (int, float)) and (price>= 0), ('param `price` '
            'should be a positive number >=0. Inputted: %s'%price)
                        
        assert isinstance (trigger_price, (int, float)) and (trigger_price>= 0), ('param `trigger_price` '
            'should be a positive number >=0. Inputted: %s'%trigger_price)
        
        assert product in (PRODUCT_MIS, PRODUCT_NRML), ('param `product` should be '
            'either "MIS" or "NRML". Inputted: %s'%product)    
            
        assert isinstance (timestamp, int) and (timestamp>= 0), ('param `timestamp` '
            'is epoch time and should be a integer >=0. Inputted: %s'%timestamp)

        try:
            message = locals()
            del(message['self'])
            message['tag'] = tag[:16] if isinstance(tag, str) else tag
            message['strategy_id'] = strategy_id[:16]
            message['message_id'] = self.message_id
        
            self.socket.send_json(message)
            self.message_id += 1
            return message['message_id']
        except:
            logger.exception(message)
            return 0 
            
    def stop(self):
        """
        Stop this `local_client`. Run it at EOD only. Even if you don't stop it
        you should be fine.
        
        """
        try:
            self.socket.close()
        except Exception as e:
            logger.exception(e)
        


class RabbitmqReconnectingPublisher(object):
    """
    TO understand the design pattern in rabbitmq, please go through the article
    below:
    
    https://www.cloudamqp.com/blog/2015-05-18-part1-rabbitmq-for-beginners-what-is-rabbitmq.html
    
    Essentially, the publishing app, called publisher (You) will send messages
    (over a queue) to a broker (or Exchange) which is hosted in Meru's server.
    Once a message has been sent to the Exchange, you will receive a delivery
    confirmation for the same.
    
    At the server side, once the message is recieved at exchange, Meru would
    handle the message. Essentially, it will spawn a consumer to which this 
    exchange would deliver this message. So, Essentially:
        
        Producer (you) ---(message)---> Broker (Exchange) At Meru ---(message)---> Consumer at Meru.
    
    Meru is sharing this Producer app with you, which besides publishing
    (sending) messages to Meru's server will handle unexpected interactions and 
    events with RabbitMQ such as channel and connection closures and 
    enusure that publisher app reconnects or comes back online when such events 
    happen. 
        
    Note
    ----

    1. The default implementations are done using:
        https://github.com/pika/pika/blob/master/examples/asynchronous_publisher_example.py
        https://github.com/pika/pika/blob/master/examples/asynchronous_consumer_example.py
        
        
    AMQP
    ----
    Authoritative and comprehensive AMQP documenatation is available on:
        https://www.rabbitmq.com/amqp-0-9-1-reference.html
        
    rerfer to link to find out more on message properties.
        https://livebook.manning.com/book/rabbitmq-in-depth/chapter-3/101        

    """

    def __init__(self, name=NAME, host=HOST, 
                 port=PORT, exchange=EXCHANGE_NAME, 
                 exchange_type='direct', virtual_host=VIRTUAL_HOST,
                 user=USER, password=PASSWORD, routing_key=ROUTING_KEY,
                 app_id=APP_ID, dealer_id=DEALER_ID, 
                 delivery_confirmations=True):
        """
        Setup the publisher object to connect to RabbitMQ server hosted at 
        Meru's servers.
        
        Parameters
        ----------
        exchange : str, optional
            The name of the exchange. 
            The default is EXCHANGE_NAME defined at top level.

        exchange_type : str, optional
            type of exchange eg. direct, topic etc. Refer Rabbitmq documentation.
            The default is 'direct'.
            
        name : str, optional
            name for this Publisher. It will be used in the name of the thread
            running this publisher. 
            
            The default is NAME defined at top level.

        delivery_confirmations : boolean, optional

            Whether delivery confirmation is enabled or not. If enabled, you 
            will get a confirmation when a message sent by you is recieved by 
            Exchange.
            
            So, if messages are not confirmed withing a certain period (say 5
            seconds), one could assume that message was lost and retry sending.
            However, the Exchange will only send confirmation when certain
            conditions are met, and in very rare cases can take long.

            The default is True.

        Returns
        -------
        None.

        Note
        ----
        1. It will store the basic params for this connection.
        2. It will create connection and channel and connect to the exchange 
            when `connect` method is called.
        3. It needs the exchange to be declared beforehand at Meru's  server, 
            else it will throw errors as the user for this publisher doesn't have
            access rights to exchange.
            
        """
        credentials = pika.PlainCredentials(USER, PASSWORD)
        self.parameters = pika.ConnectionParameters(
                host=HOST,
                port=PORT,
                virtual_host=VIRTUAL_HOST,
                credentials=credentials,
                connection_attempts=3)         
        
        self._exchange = exchange
        self._exchange_type = exchange_type
        self.app_id = app_id
        self.routing_key = routing_key
        self._delivery_confirmations = delivery_confirmations     
        self._name = self.__class__.__name__ if name is None else name       
        
        
        self._connection = None
        self._connection_no = 0
        """
        int, 
        Initialized at 0. But first connection will have the value of 1 as it
        is incremented before connection is made.
        
        To keep a count of connection number in case of multiple reconnections.
        """
        
        self._channel = None

        self._deliveries = None
        self._acked = None
        self._nacked = None
        self._delivery_tag = None

        self._stopping = False
        
        # will be used to seed the `user` property in message.
        self._user = USER
        """
        str
        will be used to seed the `user` `property` in message.
        """
        
        # will be used to see the `type` property in message.
        self._type = 'SIGNAL'
        """
        str
        will be used to set the `type` `property` in message.
        """
        
        self.dealer_id = dealer_id
        """
        str
        
        dealer_id assigned to you by us. This will be auto appended to
        the messages sent by you and allow us to identify which partner
        has sent the signal.
        
        """
        
        self.message_mapper = dict()
        """
        dict.
        
        This reconnecting publisher reconnects by creating a new connection 
        whenever there is a unexpected disconnection. Under the hood, the 
        delivery confirmation mechanism implemented by RabbitMq uses an invisible
        field called `deliver_tag` which is maintained internally by the library.
        
        When a new connection is made, the delivery_tag initializes at 0 and
        then increases by 1 for each message. We maintain `self._delivery_tag`
        attr which exactly equals the internal `delivery_tag` maintained by the
        library and use `self._delivery_tag` for confirming delivery.
        
        When a reconnection happens after an unexpected disconnection, then this
        `delivery_tag` or `self._delivery_tag` is reset to 0. So, 
        `self._delivery_tag` can't be used as a unique key for a message sent.
        
        We will use `message_id` maintained by the `ZMQLocalClient` as unique
        key. Now, the server only returns `delivery_tag` while confirming delivery
        and thus we need a mapper to map a `delivery_tag` to a `message_id`.
        
        key: tuple of (`self_connection_no`, `self._delivery_tag`)
        value : `message_id` which is unique for each message sent.
        
        """
        
        self.messages_pending = dict()
        """
        dict
        
        dict of messages which are not yet confirmed by the Broker to
        this publisher. 
        
        When a message in being published to Broker, it it will added to this
        dict and remove as soon as broker sends a delivery confirmation. The
        delivery confirmation is very fast. So, if any message remains in 
        this dict stays beyond 5 seconds or so, then we have a problem. 
        
        key: 'message_id', which is `message_id` for this publisher. 
        
        value: python dict represting the message.
        
        """
        
        self.message_id_last = 0
        """
        int
        
        The message_id of the last message recieved by the publisher.
        """

    @property
    def is_open(self):
        try:
            return self._connection.is_open
        except:
            return False

    def connect(self):
        """
        This method connects to RabbitMQ, returning the connection handle.
        When the connection is established, the on_connection_open method
        will be invoked by pika.        

        Returns
        -------
        pika.SelectConnection
            pika.SelectConnection connection.

        """

        logger.debug('%s Rabbitmq: connecting..'%(self._name))
        return pika.SelectConnection(
            parameters= self.parameters,
            on_open_callback=self.on_connection_open,
            on_open_error_callback=self.on_connection_open_error,
            on_close_callback=self.on_connection_closed)

    def on_connection_open(self, _unused_connection):
        """
        This method is called by pika once the connection to RabbitMQ has
        been established. It passes the handle to the connection object in
        case we need it, but in this case, we'll just mark it unused.

        Parameters
        ----------
        _unused_connection : pika.SelectConnection 
            pika.SelectConnection object

        Returns
        -------
        None.

        """
        logger.debug('%s Rabbitmq Connection opened!'%self._name)
        self.open_channel()

    def on_connection_open_error(self, _unused_connection, err):
        """
        This method is called by pika if the connection to RabbitMQ
        can't be established.

        Parameters
        ----------
        _unused_connection : pika.SelectConnection 
            pika.SelectConnection 
            
            
        err : Exception 
            The Error.

        Returns
        -------
        None.

        """
        logger.error('%s Rabbitmq connection open failed, '
                     'reopening in 5 seconds: %s'%(self._name, err))
        self._connection.ioloop.call_later(5, self._connection.ioloop.stop)

    def on_connection_closed(self, _unused_connection, reason):
        """
        This method is invoked by pika when the connection to RabbitMQ is
        closed unexpectedly. Since it is unexpected, we will reconnect to
        RabbitMQ if it disconnects.

        Parameters
        ----------
        _unused_connection : pika.connection.Connection 
            The closed connection obj.
            
        reason : Exception
            exception representing reason for loss of
            connection.

        Returns
        -------
        None.

        """
        self._channel = None
        if self._stopping:
            self._connection.ioloop.stop()
        else:
            logger.warning('%s Rabbitmq Connection closed, reopening in 5 seconds: %s'%(self._name, reason))
            self._connection.ioloop.call_later(5, self._connection.ioloop.stop)

    def open_channel(self):
        """
        This method will open a new channel with RabbitMQ by issuing the
        Channel.Open RPC command. When RabbitMQ confirms the channel is open
        by sending the Channel.OpenOK RPC reply, the on_channel_open method
        will be invoked.
        
        Returns
        -------
        None.

        """
        logger.debug('%s Rabbitmq Creating a new channel.'%self._name)
        self._connection.channel(on_open_callback=self.on_channel_open)

    def on_channel_open(self, channel):
        """
        This method is invoked by pika when the channel has been opened.
        The channel object is passed in so we can make use of it.

        Since the channel is now open, we'll declare the exchange to use.

        Parameters
        ----------
        channel : pika.channel.Channel
            The channel object.

        Returns
        -------
        None.

        """
        logger.info('%s Rabbitmq Publishing Channel opened!'%self._name)
        self._channel = channel
        self.add_on_channel_close_callback()
        
        # Try to place any messages_pending as well.
        self.republish_pending_messages()
        
        # since this user doesn't have rights to setup up exchange, we
        # will not attempt to set it up.
        # if self._exchange != "":
        #     self.setup_exchange()

    def add_on_channel_close_callback(self):
        """
        This method tells pika to call the on_channel_closed method if
        RabbitMQ unexpectedly closes the channel.

        Returns
        -------
        None.

        """
        logger.debug('%s Rabbitmq Adding channel close callback.'%self._name)
        self._channel.add_on_close_callback(self.on_channel_closed)

    def on_channel_closed(self, channel, reason):
        """
        Invoked by pika when RabbitMQ unexpectedly closes the channel.
        Channels are usually closed if you attempt to do something that
        violates the protocol, such as re-declare an exchange or queue with
        different parameters. In this case, we'll close the connection
        to shutdown the object.

        Parameters
        ----------
        channel : pika.channel.Channel
            The closed channel.
        reason : Exception
            why the channel was closed

        Returns
        -------
        None.

        """       
        logger.warning('%s Rabbitmq Channel %s was closed: %s'%(self._name, 
                                                        channel, reason))
        self._channel = None
        if not self._stopping:
            self._connection.close()

    def setup_exchange(self):
        """
        Setup the exchange on RabbitMQ by invoking the Exchange.Declare RPC
        command. When it is complete, the on_exchange_declareok method will
        be invoked by pika.

        The name of the exchange to declare is passed as `exchange` input at the
        time of initializaion of this object.
        
        It will not be used by this publisher as the user doesn't have sufficient
        user rights.

        Returns
        -------
        None.

        """
        logger.debug('%s Rabbitmq Declaring exchange %s'%(self._name, self._exchange))
    
        self._channel.exchange_declare(
            exchange=self._exchange,
            exchange_type=self._exchange_type,
            callback=self.on_exchange_declareok,
            passive=False, durable=False, auto_delete=False
            )

    def on_exchange_declareok(self, _unused_frame):
        """
        Invoked by pika when RabbitMQ has finished the Exchange.Declare RPC
        command.
        
        Parameters
        ----------
        _unused_frame : pika.Frame.Method 
            Exchange.DeclareOk response frame.

        Returns
        -------
        None.

        """

        logger.debug('%s Rabbitmq Exchange declared: %s'%(self._name, 
                                                          self._exchange))
        if self._delivery_confirmations:
            self.enable_delivery_confirmations()
            
    def enable_delivery_confirmations(self):
        """
        Send the Confirm.Select RPC method to RabbitMQ to enable delivery
        confirmations on the channel. The only way to turn this off is to close
        the channel and create a new one.

        When the message is confirmed from RabbitMQ, the on_delivery_confirmation 
        method will be invoked passing in a Basic.Ack or Basic.Nack method from 
        RabbitMQ that will indicate which messages it is confirming or rejecting.

        Returns
        -------
        None.

        """
        logger.debug('%s Rabbitmq Issuing Confirm.Select RPC command'%self._name)
        self._channel.confirm_delivery(self.on_delivery_confirmation)

    def on_delivery_confirmation(self, method_frame):
        """
        Invoked by pika when RabbitMQ responds to a Basic.Publish RPC
        command, passing in either a Basic.Ack or Basic.Nack frame with
        the delivery tag of the message that was published. The delivery tag
        is an integer counter indicating the message number that was sent
        on the channel via Basic.Publish. Here we're just doing house keeping
        to keep track of stats and remove message numbers that we expect
        a delivery confirmation of from the list used to keep track of messages
        that are pending confirmation.


        Parameters
        ----------
        method_frame : pika.frame.Method 
            Basic.Ack or Basic.Nack frame.

        Returns
        -------
        None.

        """
        confirmation_type = method_frame.method.NAME.split('.')[1].lower()
        delivery_tag = method_frame.method.delivery_tag
        logger.debug('%s Rabbitmq  Received %s for delivery tag: %i.'%(
                self._name,
                confirmation_type,
                delivery_tag))
        if confirmation_type == 'ack':
            self._acked += 1
            message_id = self.message_mapper[(self._connection_no, delivery_tag)] 
            message = self.messages_pending.pop(message_id, None)        
            tag = message.get('tag', None)

        elif confirmation_type == 'nack':
            self._nacked += 1
            
            
        self._deliveries.remove(method_frame.method.delivery_tag)
        logger.debug(
            '%s Rabbitmq Published %i messages, %i have yet to be confirmed, '
            '%i were acked and %i were nacked', self._name, self._delivery_tag,
            len(self._deliveries), self._acked, self._nacked)
        logger.info("Received delivery confirmation for message with message_id: %s and "
                    "tag: %s!"%(message_id, tag))

    def publish_message(self, message):
        """
        Publish the message to the Meru's server.

        Parameters
        ----------
        message : Dictionary
            Dict representing a message which you want to send to 
            Meru servers.
            
            This app will add a few fields listed below while sending 
            the messages to Meru. If your existing messages have these 
            fields, they will be overridden here.
            
                `dealer_id`: dealer_id assigned to you by us. It will help us 
                    indentify which partner sent us this message.
                    
        Returns
        -------
        None.

        Note
        ----        
        Pulishing over a new callback gives better performance over applications
        which are launched as multiprocessing processes. Otherwise directly
        publishing leads to a lot of error messages, though the messages get
        published. 
        
        If this implementation turns out to be an issue, just remove everything
        and use the default last line!
                
        """
        cb = functools.partial(self._publish_message, message)
        self._connection.ioloop.add_callback_threadsafe(cb)
#        self._connection.ioloop.call_later(0, cb)
#        self._publish_message(message)
        
    def _publish_message(self, message):
        """
        Publish a message to RabbitMQ.
        
        If _delivery_confirmations is enabled, append a list of deliveries 
        with the message number that was sent. This list will be used to check 
        for delivery confirmations in the on_delivery_confirmations method.

        Parameters
        ----------
        message : dictionary
            Message to be sent to Meru.
            
        Returns
        -------
        None.

        """
        try:
            if self._channel is None or not self._channel.is_open:
                return
    
            #  user_id property is key authentication mechanism! Meru has
            # provided you are user_id and if you put something else here
            # the message will automatically be rejected by Rabbitmq.
            properties = pika.BasicProperties(
                    app_id=self.app_id,    
                    content_type='application/json',
                    type=self._type,
                    user_id=str(self._user),
                    timestamp=int(datetime.datetime.now().timestamp() * 1000000),
                    message_id=str(self._delivery_tag),
                    )                   
                                        
            assert 'strategy_id' in message, ("field `strategy_id` missing in "
                    "message! it is cumpulsory! Ignoring this message: %s"%message)
            
            assert 'ticker' in message, ("field `ticker` missing in "
                    "message! it is cumpulsory! Ignoring this message: %s"%message)
            
            assert 'action' in message, ("field `strategy_id` missing in "
                    "message! it is cumpulsory! Ignoring this message: %s"%message)
            
            assert 'quantity' in message, ("field `quantity` missing in "
                    "message! it is cumpulsory! Ignoring this message: %s"%message)
            
            assert 'message_id' in message, ("field `message_id` missing in "
                    "message! Some error in `ZMQLocalClient` client shipped by "
                    "Meru Captials! Ignoring this message.\n\n**Please contact "
                    "Meru Capitals!**\n\nThe message is: %s"%message)

            # We add the following fields to the message:
            message['timestamp'] = message.get('timestamp', 
                                int(datetime.datetime.now().timestamp() * 1000000)
                                )
            tag = message.get('tag', None)
            message['tag'] = tag[:16] if isinstance(tag, str) else tag
            message['price'] = message.get('price', 0)
            message['trigger_price'] = message.get('trigger_price', 0)
            message['product'] = message.get('product', PRODUCT_MIS)
                        
            # dealer_id will neccessarily be added here, and overridden
            # if provided by you
            message['dealer_id'] = self.dealer_id
            message_id = message['message_id']
            self.message_id_last = message_id 
            
            
            self._channel.basic_publish(
                    exchange=self._exchange, 
                    routing_key=self.routing_key,
                    body=json.dumps(message),
                    properties=properties)
    
            if self._delivery_confirmations:
                self._delivery_tag += 1
                self._deliveries.append(self._delivery_tag)
                # map the delivery_tag to message_id of the message
                self.message_mapper[(self._connection_no, self._delivery_tag)] =  message_id
                self.messages_pending[message_id] = message

                logger.debug('%s Rabbitmq Published message # %i'%(self._name, self._delivery_tag))
            
            logger.info("Successufully Published message with message_id: %s, tag: "
                        "%s, message: %s!"%(message_id, tag, message))

        except:
            logger.exception(message)
            
    def republish_pending_messages(self):
        """
        Re-publish the pending messages again for which delivery confirmations
        haven't been received so far!
        
        When the message is republished it will acquire a new 
        (self._connection_no, self._delivery_tag) combo and a new entry will
        be made in `self.message_mapper` dict. However, on delivery confirmation
        this `self.message_mapper` dict will point to the `message_id` of the 
        message and the message will be popped out from `self.messages_pending`!
        
        We will have many keys in `self.message_mapper` pointing to same
        `message_id` but that should be fine!

        Returns
        -------
        None.

        """

        for message_id, message in self.messages_pending.items():
            self.publish_message(message=message)
            
        
    def delete_exchange(self, exchange=None):
        """
        Delete the exchange.
        
        Not used in this publisher as user doens't have access rights.

        Parameters
        ----------
        exchange : str, optional
            The name of the exchange to be deleted. 
            The default is '' which represents default exchange.

        Returns
        -------
        None.

        """
        exchange = exchange or self._exchange 
        self._channel.exchange_delete(exchange=exchange)
        logger.warning('%s Rabbitmq deleting exchange %s.', self._name, exchange)                
 
    def run(self):
        """
        Connect to exchange and start the IOLoop to publish the messages.
        Also, monitor any disconnections and do any reconnects if required. 
        
        This is blocking in nature.

        Returns
        -------
        None.

        """
        while not self._stopping:
            self._connection = None
            self._deliveries = []
            self._acked = 0
            self._nacked = 0
            # For each connection delivery_tag is reset to 1 and hence 
            # we have to reset it. The combo of (_delivery_tag, _connection_no)
            # should be unique though.
            self._delivery_tag = 0
            self._connection_no += 1

            try:
                self._connection = self.connect()
                self._connection.ioloop.start()
            except KeyboardInterrupt:
                self.stop()
                if (self._connection is not None and
                        not self._connection.is_closed):
                    # Finish closing
                    self._connection.ioloop.start()

        logger.debug('%s Stopped.'%self._name)

    def stop(self):
        """
        Stop the publisher by closing the channel and connection.
        We call the actual stop method in a timer thread to allow any pending
        messages to be published first.

        Returns
        -------
        None.

        """
        logger.debug('%s Rabbitmq Stopping!', self._name)
        self._stopping = True
        self.stop_thread = threading.Timer(interval=5, function=self._stop)
        self.stop_thread.start()
#        self.stop_thread.join(timeout=8)
        
        
    def _stop(self):
        """
        Do the actual work of stopping the publisher by closing the channel and
        then the econnection.

        Returns
        -------
        None.

        """
        self.close_channel()
        self.close_connection()

    def close_channel(self):
        """
        Close the channel with RabbitMQ by sending the Channel.Close RPC command.

        Returns
        -------
        None.

        """
        if self._channel is not None:
            logger.debug('%s Rabbitmq closing the channel', self._name)
            self._channel.close()

    def close_connection(self):
        """
        Close the connection with RabbitMQ.

        Returns
        -------
        None.

        """
        if self._connection is not None:
            logger.info('%s Rabbitmq closing connection', self._name)
            self._connection.close()
            
            
    def run_in_thread(self):
        """
        Connect to the exchange and start the IOLoop to publish messages. 
        Also, monitor any disconnections and do any reconnects in a separate thread. 

        This is non-blocking in nature.
        
        Returns
        -------
        None.

        """
        self.threadname = "MERU.%s.%s.Rabbitmq.Publisher"%(self._exchange, self._name)
        self.thread = threading.Thread(
            target=self.run,
            name=self.threadname,
            daemon=True
            )
        self.thread.start()
            

class MeruApp(object):
    """
    `MeruApp` will sends the final messages to exchange. This is the app which
    you will run.
    
    It will:
        a. have an attr 'rpublisher`: `RabbitmqReconnectingPublisher` object.
            It:
                a. auto restarts and reconnects on rabbitmq originated errors. 
                b. connects to Meru server when run 
                c. Does actual sending of messages to Meru's server.
                c. recieves delivery confirmation from Meru server, 
                    when the Meru server receives a message sent by it.
            
        b. zmq: The ZMQ reciever 
        
        c. This app will recieve all messages (orders) sent by you trading 
            applications on `zmq` and then send to Meru server over 
            `rpublisher`.
            
        d. Its `rupublisher` attr can be used to track delivery confirmations\
            pending messages etc.
    """
    
  
    def __init__(self, port_local=PORT_LOCAL,
                 rabbitmq_exchange=EXCHANGE_NAME):     
        """
        Init this `MeruApp`. Most of the parameters are defined at constant
        at top level in this app.

        Parameters
        ----------
        port_local :  int, optional
            The port to which your trading app will send messages to! It will 
            be listened to by ZMQ.
            
            The default is PORT_LOCAL defined at top level.
            
        rabbitmq_exchange : str, optional
            The name of the exchange. 
            The default is EXCHANGE_NAME defined at top level.
            
        Returns
        -------
        None.

        """
        self.port_local = port_local
        self.is_monitor_local_client = threading.Event()
        self.rabbitmq_exchange = rabbitmq_exchange

        # Create a RabbitmqReconnectingPublisher instance to communicate with
        # Meru's servers. 
        self.create_rpublisher()
       
        # Create a `local_client` listening to messages from main trading 
        # application over ZMQ. 
        self.create_local_client() 

        self.__version__ = 'v1a'

    @property
    def message_id_last(self):
        """
        Current message_id in the `local_client`
        """
        return self.rpublisher.message_id_last
    
    
    @property
    def messages_pending(self):
        """
        The messages still waiting to be confirmed, most likely they have
        been not been recieved by Meru Servers!.
        """
        return self.rpublisher.messages_pending


    def create_rpublisher(self):
        """
        Create a `RabbitmqReconnectingPublisher` class instance to communicate with
        Meru's servers. 

        Returns
        -------
        None.

        """
        self.rpublisher = RabbitmqReconnectingPublisher(
            exchange=self.rabbitmq_exchange)
        
    def create_local_client(self):
        """
        Initialize the  `local_client` listening to messages from your main 
        trading app over ZMQ and bind this instance to its port!

        Returns
        -------
        None.

        """
        try:
            context = zmq.Context()
            self.local_client = context.socket(zmq.PAIR)
            self.local_client.bind("tcp://*:%s" % self.port_local)
            time.sleep(2)
        except Exception as e:
            logger.exception(e)
            
        
                
    def start_rpublisher(self):
        """
        Start the Rabbitmq publisher
        """
        self.rpublisher.run_in_thread()
        

    def start_local_client(self):
        """
        Start the `local_client` which will start listening to messages sent by 
        your trading app over ZMQ port defined in `self.port_local`

        Returns
        -------
        None.

        """
        try:
            logger.warning("Starting the MERU ZMQ Client!")
            self.is_monitor_local_client.set()
            self.thread_local = threading.Thread(
                target=self.on_message_local_client,
                name='MERU.ZMQ.Client.Local', 
                daemon=True
                )
            self.thread_local.start()            
        except Exception as e:
            logger.exception(e)          
        
            
    def on_message_local_client(self):
        """
        Callback run when a message sent by the main trading application is
        recieved by the `local_client`.
        
        Essentially, it will resend the message to the Meru servers after
        appending a `message_id` and `dealer_id` and little bit of processing.

        Returns
        -------
        None.

        """
        while self.is_monitor_local_client.is_set():
            try:
                message = self.local_client.recv_json()
                self.publish_message_to_meru(message=message)
            except Exception as e:
                logger.exception(e)
            
            
    def publish_message_to_meru(self, message):
        """
        Publish a message to Meru's servers!

        Parameters
        ----------
        message : dict, optional
            The params passed by trading application to sent to Meru in
            method call `meru_place_order` in its `ZMQLocalClient` attr
            are converted into a dict. This dict is passed here as message.
            

        Returns
        -------
        None.

        """
        self.rpublisher.publish_message(message=message)
        
    def stop_rpublisher(self):
        """
        Stop the Rabbitmq publisher. 
        """
        self.rpublisher.stop()
        
    def stop_local_client(self):
        """
        Stop the ZMQ listner listening to messages from your trading app 
        over local port.

        Returns
        -------
        None.

        """
        try:    
            # clear the flag.
            self.is_monitor_local_client.clear()        
            self.thread_local.join()
        except Exception as e:
            logger.exception(e)            
        
        
    def start_handler(self):
        """
        Start the Rabbitmq publisher as well as ZMQ listener!

        Returns
        -------
        None.

        """
        try:
            logger.warning("Starting the MERU ZMQ Client!")
            self.start_local_client()
            self.start_rpublisher()
            time.sleep(2)
        except Exception as e:
            logger.exception(e)      
            
    def stop_handler(self):
        """
        Stop this Rabbitmq Publisher as well as ZMQ listener.
        
        It will also kill the thread which monitors and reconnects
        this client in case of disconnection.
        
        When this client is interfering with the code, just run this method and
        get rid of ZMQ client!

        Returns
        -------
        None.

        """
        try:    
            # clear the flag.
            self.stop_rpublisher()
            self.stop_local_client()
        except Exception as e:
            logger.exception(e)     
            
    def republish_pending_messages(self):
        """
        Publish the pending messages again for which delivery confirmations
        haven't been received so far!        

        Returns
        -------
        None.

        """
        self.rpublisher.republish_pending_messages()
        pass

        #%%


if __name__ == '__main__':
    import logging
    logger = logging.getLogger('MeruApp')
    logger.setLevel(logging.INFO)
    
    """
    Logging will be done at two places:
        a. Regular Console
        b. To a file names MeruApp_2021-02-02.log or similar, changing as per
            date and stored in $SHOME for your platform.
    """

    formatter = "%(asctime)s.%(msecs)03d | %(module)s | %(levelname)s | %(lineno)d | %(message)s"
    DATE_FORMAT = '%H:%M:%S'    
    formatter_obj = logging.Formatter(fmt=formatter, datefmt=DATE_FORMAT)
    
    # add a file_handler to log to a file.
    date = datetime.datetime.today().strftime("%Y-%m-%d")
    # using ~ as path for log as it can be resolved in all platforms.
    log_path = os.path.expanduser(os.path_join("~", "MeruApp_%s.log"%date))
    fh = logging.FileHandler(log_path)
    fh.setFormatter(formatter_obj)
    fh.setLevel(logging.INFO)
    
    # stream_handler ie logging to console.    
    ch = logging.StreamHandler()
    ch.setFormatter(formatter_obj)
    ch.setLevel(logging.INFO)    
    
    logger.addHandler(fh)
    logger.addHandler(ch)      
    
    
    # logging.basicConfig(format=formatter, datefmt=DATE_FORMAT)
    # logger = logging.getLogger(__name__)
    # logger.setLevel(logging.DEBUG)
 
    pub = MeruApp()
    pub.start_handler()
    Z = self = pub.rpublisher
    
    # the pending messages, for which delivery confirmation has not been recieved
    # can be checked:
    pub.rpublisher.messages_pending
    # pub.republish_pending_messages()
    

#    pub.stop_handler()
