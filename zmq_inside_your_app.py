#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 31 12:46:43 2021

@author: saurav
"""

from kanhoji_zmq.rabbitmq_publisher import ZMQLocalClient
import time
import logging
import datetime
logger = logging.getLogger(__name__)

class MyApp(object):
    def __init__(self, sample_param1=0, sample_param2=1):
        """
        Regular initialization of your trading app. Say iit has two init params
        decided by you:
            sample_param1  and sample_param1 
            
        All we need to to do is add an attr in your app called `zmq_client`
        as shown below in last line of this init code.

        Parameters
        ----------
        sample_param1 : TYPE, optional
            DESCRIPTION. The default is 0.
        sample_param2 : TYPE, optional
            DESCRIPTION. The default is 1.

        Returns
        -------
        None.

        """
        self.sample_param1 = sample_param1
        self.sample_param2 = sample_param2
        
        # We only need you to add this attr!
        # Whenever you need to place an order call the method 
        # `self.zmq_client.meru_place_order`
        self.zmq_client = ZMQLocalClient()

    def meru_place_order(self,
            ticker='BANKNIFTY21JAN30600CE', 
            action='SELL', 
            strategy_id='ALGO1',
            quantity=0,
            price=0,
            trigger_price=0,
            product='MIS',
            tag=None,
            timestamp=int(datetime.datetime.now().timestamp() * 1000000)
            ):
        """
        Demostration of placing order to Merus's server!
        
        params are explained in method `meru_place_order` of the 
        `ZMQLocalClient` instance in `self.zmq_client` attr.

        Parameters
        ----------
        ticker : TYPE, optional
            DESCRIPTION. The default is 'BANKNIFTY21JAN30600CE'.
        action : TYPE, optional
            DESCRIPTION. The default is 'SELL'.
        strategy_id : TYPE, optional
            DESCRIPTION. The default is 'ALGO1'.
        quantity : TYPE, optional
            DESCRIPTION. The default is 0.
        price : TYPE, optional
            DESCRIPTION. The default is 0.
        trigger_price : TYPE, optional
            DESCRIPTION. The default is 0.
        product : TYPE, optional
            DESCRIPTION. The default is 'MIS'.
        tag : TYPE, optional
            DESCRIPTION. The default is None.
        timestamp : TYPE, optional
            DESCRIPTION. The default is int(datetime.datetime.now().timestamp() * 1000000).

        Returns
        -------
        None.

        """
        # A signal  genrated using your custom logic
        return self.zmq_client.meru_place_order(ticker=ticker, action=action, 
            strategy_id=strategy_id, quantity=quantity, price=price, 
            trigger_price=trigger_price, product=product, tag=tag,
            timestamp=timestamp)
        
#%%
if __name__ == "__main__":
    # Initialize logging
    import logging
    formatter = "%(asctime)s.%(msecs)03d | %(module)s | %(levelname)s | %(lineno)d | %(message)s"
    DATE_FORMAT = '%H:%M:%S'    
    logging.basicConfig(format=formatter, datefmt=DATE_FORMAT)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)   
    
    myapp = MyApp()
    # myapp.meru_place_order()
            
        
        
    
        
        