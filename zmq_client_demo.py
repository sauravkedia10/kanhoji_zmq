################################################
#%% Section 1: Initial Activity to install pyzmq
################################################

# 1. Intsall zmq (ZeroMQ) on your machine.
# on ubuntu Terminal:
# $ sudo apt-get install libzmq3-dev

# 2. Install its python library, pyzmq using pip
# $ pip install pyzmq
     
# This will setup Zeromq messaging platform on you machine and it can be used
# to send and recieve messages on various ports. One port (say server) could be 
# part of one app while the other port (say client) could be a part of another
# app. Thus it can help in distributed programming to commnicate between various
# processes/applications.

# Both client and server side app are demo in this single file below.
# Open two terminals in your machine and in both run python interpretors.
# In 1st terminal, run the server side app first by copying and pasting that code

# then in second terminal, run the client side app by copying and running that code

##########################################    
#%% Section 2:  client side demo app
##########################################
# the client side is the code you will run on you machine while we will run the server 
# side code in live enironment. For current purpose of testing, you will run both
# the client and server side on your machine in two interpretors as explained earlier in
# previous section.

import threading
from zmq.auth.thread import ThreadAuthenticator
import zmq

# we have provided the host, username and password for this demo below.
# In live environment, we will provide you our ip (host, port), username and password.
# this app can comminicate with external ip without any change in code.

host = '127.0.0.1'
port = 5557
ctx = zmq.Context.instance()
client = ctx.socket(zmq.REQ)
client.plain_username = b'admin'
client.plain_password = b'secret'

#  open client Socket to talk to server
print("Connecting to hello world server…")
client.connect("tcp://%s:%s"%(host, port))

# smaple function to send message
def send_message(payload):
    # send json to server.
    # in live enivronment, you will send you message with order details in form
    # of a python dictionary. A sample dictionary is provided in this example but
    # realtime order will have a lot more fields. 
    client.send_json(payload)
    # once client sends the message over Zmq, it will block till the time server
    # sends a reply.
    message = client.recv_json()
    print("Received reply [ %s ]" % (message))

# sample order message
# the field message_code can be maintained at your end to track the messages you 
# are sending. Server will send an acknowledgement along with this message code.
# This will help you track which messages were delivered successfully to server. 
order_info = dict(
    ticker = 'BANKNIFTY21JAN30600CE',
    action = 'BUY',
    quantity = 25,
    strategy_code = 'ALGO1',
    dealer = 'PARTNER1',
    message_code = 1
    )

# send the order_info
send_message(payload=order_info)

# Since the execution will be blocked till response is recieved from server,
# you can send the payload using in a seperare thread
order_info['message_code'] = 2
thread = threading.Thread(target=send_message, args=(order_info,))
thread.start()

# TO inform server that all tasks are over for the day, send STOP message.
send_message(payload='STOP')

# at End of Day, close the client.
client.close()
    
####################################
#%% Section 3: server side demo app.
####################################
# This will be mainaitned by us at our end, here it just for demo.
import time
import zmq
from zmq.auth.thread import ThreadAuthenticator
import threading

passwords={'admin': 'secret'}
# For production host will remain as '127.0.0.1', no need to change to ip of 
# the server. We will use server.bind to use to listen to all external ips.
host = '127.0.0.1'
port = 5557

ctx = zmq.Context.instance()
auth = ThreadAuthenticator(ctx)
auth.start()
auth.configure_plain(domain='*', passwords=passwords)
server = ctx.socket(zmq.REP)
server.plain_server = True  # must come before bind
# * below means listen to all hosts: local, internal as well as external ips.
server.bind('tcp://*:%s'%port)

def on_message():
    # we run an blocking infinite loop to listen to messages from client and that loop will
    # be exited when STOP is recieved. Ideally it will be run in a seperate thread
    # to make it non-blocking
    while True:
        #  Wait for next request from client
        message = server.recv_json()
        print("Received request: %s" % message)
        
        # If 'STOP' is received from client, stop this listner.
        if message == 'STOP':
            server.send_json("stopping")
            break
    
        # For other message process it by doing some work
        #  Do some 'work'
        # time.sleep(1)
    
        #  Send reply back to client
        # in simple cases, it will be just an acknowledgement that message was received by server.
        try:
            server.send_json(
                dict(status='success', message_code=message.get('message_code', None))
                )
        except Exception as e:
            print (e)
            
            # "Recieved Message with message_code: %s"%message['message_code']) 

thread = threading.Thread(target=on_message, name='Kanhoji.ZMQ.server')
thread.start()

auth.stop()
server.close()

######
# End
######