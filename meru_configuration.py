#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 10:03:16 2021

@author: saurav
"""

# all these values will pe provided by Meru and hardcoded.
PORT_LOCAL = 5556
HOST = '127.0.0.1'
PORT = 5672
VIRTUAL_HOST = ''
USER = DEALER_ID = APP_ID = ROUTING_KEY = NAME = ''
PASSWORD = '' 
EXCHANGE_NAME = ''
# EXCHANGE_NAME = ''

# CONSTANTS
PRODUCT_MIS = 'MIS'
PRODUCT_NRML = 'NRML'
